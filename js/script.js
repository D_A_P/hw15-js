'use strict';


function timeOut () {
    const text = document.getElementById('text');
    setTimeout(() => {
        text.textContent = 'Операція виконана успішно'         
    }, 3000);
}


const timerElement = document.getElementById('timer');
let count = 10;
   
const countInterval = setInterval(() => {
    timerElement.textContent = count;
    if (count === 1) {
        timerElement.textContent = 'Зворотній відлік завершено';
        clearInterval(countInterval);
      }      
      count--;
    }, 1000);



