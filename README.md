1. Метод setTimeout дозволяє запускати функцію один раз через певний інтервал часу, а метод setInterval дозволяє запускати функцію багаторазово через заданий проміжок часу, починаючи з певного інтервалу.

2. Не можна стверджувати з абсолютною впевненістю, що функції, викликані з setInterval або setTimeout,
   будуть виконані рівно через той проміжок часу, який вказали, тому що виконання може затриматися через внутрішні завдання JavaScript на виконання або інші чинники в середовищі виконання коду.

3. Щоб припинити виконання функції, яка була запланована для виклику з використанням setTimeout або setInterval,
   потрібно використати методи clearTimeout і clearInterval відповідно.
